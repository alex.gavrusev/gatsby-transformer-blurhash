# @gvrs/gatsby-transformer-blurhash

![npm (scoped)](https://img.shields.io/npm/v/@gvrs/gatsby-transformer-blurhash?style=flat-square)
![NPM](https://img.shields.io/npm/l/@gvrs/gatsby-transformer-blurhash?style=flat-square)
![PRs Welcome](https://img.shields.io/badge/PRs-Welcome-brightgreenn?style=flat-square)

Generate [BlurHash](https://blurha.sh/) placeholders for images.

## Configuration

```javascript
// In your gatsby-config.js
module.exports = {
  plugins: [
    `gatsby-plugin-image`,
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`,
    `@gvrs/gatsby-transformer-blurhash`,
  ],
};
```

## Usage example

### Fetching data

```graphql
imageSharp {
  gatsbyImageData(
    placeholder: NONE
  )
  blurHash(
    componentX: 3,
    componentY: 4,
    width: 32
  ) {
    base64Image
    hash
  }
}
```

### Rendering

```tsx
import { GatsbyImage } from "gatsby-plugin-image";

const Image = ({ gatsbyImageData, blurHash }) => (
  <GatsbyImage
    image={{
      ...gatsbyImageData,
      placeholder: {
        fallback: blurHash.base64Image,
      },
    }}
  />
);
```

## Parameters

- `width` (int, default: 32)
- `componentX` (int, between 1 and 9, default: 3)
- `componentY` (int, between 1 and 9, default: 4)

## Returns

- `hash` (string) — base 83 data returned by BlurHash
- `base64Image` (string) — base 64 encoded image

## Additional documentation

- [CHANGELOG](https://gitlab.com/alex.gavrusev/gatsby-transformer-blurhash/-/blob/master/CHANGELOG.md)
- [BlurHash](https://github.com/woltapp/blurhash)

## Prior art, inspiration

- [@m5r/gatsby-transformer-blurhash](https://github.com/m5r/gatsby-transformer-blurhash)
- [gatsby-transformer-sqip](https://github.com/gatsbyjs/gatsby/tree/master/packages/gatsby-transformer-sqip)

## See also

- [gatsby-transformer-image-mask](https://gitlab.com/alex.gavrusev/gatsby-transformer-image-mask/) — remove ragged edges from placeholders
