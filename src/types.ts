import type { FileSystemNode } from "gatsby-source-filesystem";

export type FieldArgs = {
  componentX: number;
  componentY: number;
  width: number;
};

export type NodeWithParent = FileSystemNode & { parent: string };
