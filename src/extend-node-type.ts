import type { SetFieldsOnGraphQLNodeTypeArgs, PluginOptions } from "gatsby";
import type { FileSystemNode } from "gatsby-source-filesystem";
import { GraphQLObjectType, GraphQLString, GraphQLInt } from "gatsby/graphql";
import PQueue from "p-queue";
import { createContentDigest, cpuCoreCount } from "gatsby-core-utils";
import createDebug from "debug";

import type { NodeWithParent, FieldArgs } from "./types";
import generateBlurhash from "./generate-blurhash";

const queue = new PQueue({ concurrency: cpuCoreCount() });

const debug = createDebug("gatsby-transformer-blurhash");

const blurhashSharp = (
  args: SetFieldsOnGraphQLNodeTypeArgs,
  _options: PluginOptions
) => {
  const { cache, getNodeAndSavePathDependency } = args;

  return {
    blurHash: {
      type: new GraphQLObjectType({
        name: "BlurhashSharp",
        fields: {
          base64Image: { type: GraphQLString },
          mask: { type: GraphQLString },
          hash: { type: GraphQLString },
        },
      }),
      args: {
        componentX: {
          type: GraphQLInt,
          defaultValue: 4,
        },
        componentY: {
          type: GraphQLInt,
          defaultValue: 3,
        },
        width: {
          type: GraphQLInt,
          defaultValue: 32,
        },
      },
      async resolve(
        image: NodeWithParent,
        fieldArgs: FieldArgs,
        context: { path: string }
      ) {
        const file = getNodeAndSavePathDependency(
          image.parent,
          context.path
        ) as FileSystemNode;

        const { name } = file;
        const { contentDigest } = file.internal;

        const optionsHash = createContentDigest(fieldArgs);
        const cacheKey = `transformer-blurhash-${contentDigest}-${optionsHash}`;

        debug(`Request preview generation for ${name} (${cacheKey})`);

        return queue.add(async () => {
          const cachedResult: unknown = await cache.get(cacheKey);
          if (cachedResult) {
            debug(`Using cached data for ${name} (${cacheKey})`);
            return cachedResult;
          }

          try {
            debug(
              `Executing preview generation request for ${name} (${cacheKey})`
            );

            const result = await generateBlurhash(file.absolutePath, fieldArgs);
            await cache.set(cacheKey, result);
            return result;
          } catch (err: unknown) {
            throw new Error(
              `Unable to generate blurhash for ${name} (${cacheKey})`,
              { cause: err }
            );
          }
        });
      },
    },
  };
};

const extendNodeType = (
  args: SetFieldsOnGraphQLNodeTypeArgs,
  _options: PluginOptions
) => {
  const {
    type: { name },
  } = args;

  if (name === "ImageSharp") {
    return blurhashSharp(args, _options);
  }

  return {};
};

export default extendNodeType;
