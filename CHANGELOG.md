# [2.0.0](https://gitlab.com/alex.gavrusev/gatsby-transformer-blurhash/compare/v1.0.4...v2.0.0) (2023-01-16)


### Bug Fixes

* **lint:** do not limit body line max length ([96232ee](https://gitlab.com/alex.gavrusev/gatsby-transformer-blurhash/commit/96232ee7883fa1468724fd762ffac95956144429))


### Features

* **ci:** use node:18 ([2620824](https://gitlab.com/alex.gavrusev/gatsby-transformer-blurhash/commit/2620824153e7857845111ad8e6fdbb6d0b66583a))
* **deps:** require gatsby v5 ([01dc243](https://gitlab.com/alex.gavrusev/gatsby-transformer-blurhash/commit/01dc243a163662510e144f723213f71081f9e818))
* use error cause instead of message override ([2fd788d](https://gitlab.com/alex.gavrusev/gatsby-transformer-blurhash/commit/2fd788d0a847192127df9c0040b81a9f12f2acd3))


### BREAKING CHANGES

* **deps:** support for gatsby v4 dropped

## [1.0.4](https://gitlab.com/alex.gavrusev/gatsby-transformer-blurhash/compare/v1.0.3...v1.0.4) (2022-03-07)


### Bug Fixes

* **package.json:** add description ([fb4eb62](https://gitlab.com/alex.gavrusev/gatsby-transformer-blurhash/commit/fb4eb627005dcd14e666322777e2d1ae48d9b745))

## [1.0.3](https://gitlab.com/alex.gavrusev/gatsby-transformer-blurhash/compare/v1.0.2...v1.0.3) (2022-03-07)

## [1.0.2](https://gitlab.com/alex.gavrusev/gatsby-transformer-blurhash/compare/v1.0.1...v1.0.2) (2022-03-06)


### Bug Fixes

* remove engine requirement ([309ad59](https://gitlab.com/alex.gavrusev/gatsby-transformer-blurhash/commit/309ad5991127789f1319fb507a278f2cfd76ac6d))

## [1.0.1](https://gitlab.com/alex.gavrusev/gatsby-transformer-blurhash/compare/v1.0.0...v1.0.1) (2022-03-06)


### Bug Fixes

* copy license to dist ([6931b57](https://gitlab.com/alex.gavrusev/gatsby-transformer-blurhash/commit/6931b5771447bd26c5a57c2ff2da15c4a5370615))

# 1.0.0 (2022-03-06)


### Features

* initial commit ([bb9164a](https://gitlab.com/alex.gavrusev/gatsby-transformer-blurhash/commit/bb9164aa2f65f0551d592e066250ca5ffd88cdae))
